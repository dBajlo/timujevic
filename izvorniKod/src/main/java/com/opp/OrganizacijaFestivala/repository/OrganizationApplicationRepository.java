package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.OrganizationApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrganizationApplicationRepository extends JpaRepository<OrganizationApplication, Long> {
    List<OrganizationApplication> findAllByConfirmedAndFestivalId(boolean confirmed, Long festId);
    List<OrganizationApplication> findAllByOrganizerId(long id);
    List<OrganizationApplication> findAllByOrganizerIdAndConfirmed(Long organizerId, boolean confirmed);
    List<OrganizationApplication> findAllByFestivalId(long id);
    boolean existsByOrganizerIdAndFestivalId(Long orgId, Long festId);

    Optional<OrganizationApplication> findByOrganizerIdAndFestivalId(Long organizerId, Long festivalId);

}
