package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.JobApplication;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface JobApplicationRepository extends JpaRepository<JobApplication, Long> {

    List<JobApplication> findAllByPerformer(RegisteredUser performer);

    List<JobApplication> findAllByConfirmedAndJobId(boolean confirmed, Long jobId);

    List<JobApplication> findAllByJobId(Long jobId);
    
    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("UPDATE JobApplication j SET j.numPrinted=j.numPrinted+1 WHERE j.id=id")
    public int updateNumPrinted(Long id);
}
