package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.Event;
import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    List<Event> findAll();

    List<Event> findAllByOrganizer(RegisteredUser organizer);

    List<Event> findAllByOrganizerAndEndTimeAfter(RegisteredUser organizer, LocalDateTime current);

    @Transactional
    void deleteById(Long id);

    boolean existsById(Long id);

    List<Event> findAllByFestivalId(Long id);

    List<Event> findAllByOrganizerAndEndTimeBefore(RegisteredUser organizer, LocalDateTime now);
}
