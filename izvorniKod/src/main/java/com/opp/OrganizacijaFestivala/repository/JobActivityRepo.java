package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.Job_Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface JobActivityRepo extends JpaRepository<Job_Activity, Long> {
    List<Job_Activity> findAllByJobId(Long id);
    List<Job_Activity> findAllByActivityId(Long id);
    List<Job_Activity> findAllByJobIdAndActivityId(Long jId, Long aId);
}
