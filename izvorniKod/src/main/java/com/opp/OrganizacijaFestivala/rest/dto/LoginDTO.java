package com.opp.OrganizacijaFestivala.rest.dto;

import com.sun.istack.NotNull;

import javax.validation.constraints.NotEmpty;

public class LoginDTO {
    @NotNull
    @NotEmpty
    private String username;

    @NotNull
    @NotEmpty
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
