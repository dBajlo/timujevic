package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.OrganizationApplication;

public class OrgAppDTO2 {

    private Long id;

    private String organizer;

    private Long festivalId;

    private String festivalName;

    public OrgAppDTO2() {
        super();
    }

    public OrgAppDTO2(OrganizationApplication o, String organizer) {
        this.id = o.getId();
        this.organizer = organizer;
        this.festivalId = o.getFestival().getId();
        this.festivalName = o.getFestival().getFestivalName();
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public Long getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(Long festivalId) {
        this.festivalId = festivalId;
    }
}
