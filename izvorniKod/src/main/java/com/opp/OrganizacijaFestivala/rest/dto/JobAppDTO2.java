package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.JobApplication;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;

public class JobAppDTO2 {
    private boolean festivalFinished;
    //ako je festival završen onda trebamo znati koja je pobjednička prijava za licitaciju, ako je naša onda je to
    //za nas obavljeni posao
    private String performer;
    private long id;
    private String eventName;
    private String festivalName;
    private int jobOrdNum;
    private Long jobId;
    private int price;
    private String comment;
    private int numWorkers;
    private int numPrinted;
    private int jobDuration;
    private boolean confirmed;
    private String organizer;


    public JobAppDTO2(){
        super();
    }

    public  JobAppDTO2(JobApplication jobApplication, String eventName, String festivalName, int jobOrdNum,
                       boolean festivalFinished, String performer, String organizer, Long jobId){
        id = jobApplication.getId();
        this.eventName = eventName;
        this.festivalName = festivalName;
        this.jobOrdNum = jobOrdNum;
        price = jobApplication.getPrice();
        comment = jobApplication.getComment();
        numWorkers = jobApplication.getNumWorkers();
        jobDuration = jobApplication.getJobDuration();
        confirmed = jobApplication.isConfirmed();
        numPrinted=jobApplication.getNumPrinted();
        this.organizer = organizer;
        this.festivalFinished = festivalFinished;
        this.performer = performer;
        this.jobId = jobId;
    }
    
    
    public int getNumPrinted()
    {
    	return this.numPrinted;
    }
    
    public void setNumPrinted(int numPrint)
    {
    	this.numPrinted=numPrint;
    }
	

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public boolean isFestivalFinished() {
        return festivalFinished;
    }

    public void setFestivalFinished(boolean festivalFinished) {
        this.festivalFinished = festivalFinished;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public int getJobOrdNum() {
        return jobOrdNum;
    }

    public void setJobOrdNum(int jobOrdNum) {
        this.jobOrdNum = jobOrdNum;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getNumWorkers() {
        return numWorkers;
    }

    public void setNumWorkers(int numWorkers) {
        this.numWorkers = numWorkers;
    }

    public int getJobDuration() {
        return jobDuration;
    }

    public void setJobDuration(int jobDuration) {
        this.jobDuration = jobDuration;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }
}
