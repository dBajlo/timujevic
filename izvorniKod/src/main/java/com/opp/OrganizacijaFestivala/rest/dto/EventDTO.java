package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.sun.istack.NotNull;

import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;

public class EventDTO {
    @NotNull
    private String organizer;

    @NotNull
    private Long festivalId;

    @NotNull
    private String eventName;

    private String eventDescription;

    @NotNull
    private LocalDateTime startTime;
    @NotNull
    private LocalDateTime endTime;


    public Long getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(Long festivalId) {
        this.festivalId = festivalId;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
