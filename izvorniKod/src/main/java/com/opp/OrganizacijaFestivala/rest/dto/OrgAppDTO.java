package com.opp.OrganizacijaFestivala.rest.dto;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

public class OrgAppDTO {

    @NotNull
    private String organizer;

    @NotNull
    private Long festivalId;

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public Long getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(Long festivalId) {
        this.festivalId = festivalId;
    }
}
