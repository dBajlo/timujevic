package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

public class JobAppDTO {
    @NotNull
    String performer;

    @NotNull
    Long jobId;

    @NotNull
    int price;

    String comment;

    @NotNull
    int numWorkers;
    
    int numPrinted;

    @NotNull
    int jobDuration;

    public String getPerformer() {
        return performer;
    }
    
    
    public int getNumPrinted()
    {
    	return this.numPrinted;
    }
    
    public void setNumPrinted(int numPrint)
    {
    	this.numPrinted=numPrint;
    }
    

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getNumWorkers() {
        return numWorkers;
    }

    public void setNumWorkers(int numWorkers) {
        this.numWorkers = numWorkers;
    }

    public int getJobDuration() {
        return jobDuration;
    }

    public void setJobDuration(int jobDuration) {
        this.jobDuration = jobDuration;
    }
}
