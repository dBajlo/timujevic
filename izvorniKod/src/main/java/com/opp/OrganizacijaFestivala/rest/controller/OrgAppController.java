package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.model.OrganizationApplication;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.rest.dto.OrgAppDTO;
import com.opp.OrganizacijaFestivala.rest.dto.OrgAppDTO2;
import com.opp.OrganizacijaFestivala.service.OrgAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orgApps")
public class OrgAppController {
    @Autowired
    OrgAppService orgAppService;

    @PostMapping("/create")
    public OrganizationApplication createApp(@RequestBody OrgAppDTO orgAppDTO){

        return orgAppService.createOrgApp(orgAppDTO);
    }

    @GetMapping("/listApps")
    public List<OrganizationApplication> listApps(){
        return orgAppService.findAll();
    }

    @GetMapping(value = "listConfirmed/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RegisteredUser> listConfirmedApps(@PathVariable(value = "id") Long id){

        return orgAppService.findConfirmed(id);
    }

    @GetMapping("organizersUnconfirmedApps/{organizerId}")
    public List<OrgAppDTO2> organizersUnconfirmedApps(@PathVariable Long organizerId) {
        return orgAppService.organizersUnconfirmedApps(organizerId);
    }

    @GetMapping(value = "listUnConfirmed/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrgAppDTO2> listUnConfirmedApps(@PathVariable(value = "id") Long id){

        return orgAppService.findUnconfirmed(id);
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long deleteApp(@PathVariable(value = "id") Long id){
        orgAppService.delete(id);
        return id;
    }

    @PutMapping("confirm")
    public Long confirmApp(@RequestParam Long id){
        orgAppService.confirm(id);
        return id;
    }

}
