package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.Job;

import java.util.ArrayList;
import java.util.List;

//sluzi za istovremeni prijenos posla i njegovih djelatnosti
public class JobActivityDTO {
    private Job job;

    private List<Activity> activities;

    public JobActivityDTO(){
        super();
    }

    public JobActivityDTO(Job job, List<Activity> activities){
        this.job = job;
        this.activities = activities;

    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
