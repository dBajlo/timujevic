package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.sun.istack.NotNull;

import javax.validation.constraints.NotEmpty;

public class UserDTO {

    //ulogom za koju se prijavljuje (voditelj, organizator, izvođač), a potrebni su
    // korisničko ime, lozinka, ime, prezime, slika, broj mobitela i email adresa.
    @NotNull
    @NotEmpty
    private String role;

    @NotNull
    @NotEmpty
    private String username;

    @NotNull
    @NotEmpty
    private String password;

    @NotNull
    @NotEmpty
    private String first_name;

    @NotNull
    @NotEmpty
    private String last_name;

    @NotNull
    @NotEmpty
    private String phone_number;

    @NotNull
    @NotEmpty
    private String email;

    private Byte[] profilePicture;

    public UserDTO(){
        super();
    }

    public UserDTO(RegisteredUser user){
        role = user.getRole();
        username = user.getUsername();
        password = user.getPassword();
        first_name = user.getFirst_name();
        last_name = user.getLast_name();
        phone_number = user.getPhone_number();
        email = user.getEmail();
        profilePicture = user.getProfilePicture();
    }

    public Byte[] getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Byte[] profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean checkRole(){
        if (this.getRole().equals("voditelj") || this.getRole().equals("organizator") || this.getRole().equals("izvodjac")) {
            return true;
        }
        return false;
    }

}
