package com.opp.OrganizacijaFestivala.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Performer_Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long performerId;

    @NotNull
    private  Long activityId;

    public Performer_Activity(){
        super();
    }

    public Performer_Activity(Long pId, Long aId){
        performerId = pId;
        activityId = aId;
    }

    public Long getPerformerId() {
        return performerId;
    }

    public void setPerformerId(Long performerId) {
        this.performerId = performerId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activitId) {
        this.activityId = activitId;
    }
}
