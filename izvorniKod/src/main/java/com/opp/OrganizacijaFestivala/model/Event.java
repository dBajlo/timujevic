package com.opp.OrganizacijaFestivala.model;

import com.opp.OrganizacijaFestivala.rest.dto.EventDTO;
import javax.validation.constraints.NotNull;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    private RegisteredUser organizer;

    @NotNull
    private Long festivalId;

    @NotNull
    private String eventName;

    private String eventDescription;

    @NotNull
    private LocalDateTime startTime;
    @NotNull
    private LocalDateTime endTime;

    public Event(){
        super();
    }

    public Event(EventDTO dto, RegisteredUser organizer){
        this.organizer = organizer;
        this.endTime = dto.getEndTime();
        this.eventDescription = dto.getEventDescription();
        this.eventName = dto.getEventName();
        this.startTime = dto.getStartTime();
        this.festivalId = dto.getFestivalId();
    }

    public Long getId() {
        return id;
    }

    public Long getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(Long festivalId) {
        this.festivalId = festivalId;
    }

    public RegisteredUser getOrganizer() {
        return organizer;
    }

    public void setOrganizer(RegisteredUser organizer) {
        this.organizer = organizer;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
