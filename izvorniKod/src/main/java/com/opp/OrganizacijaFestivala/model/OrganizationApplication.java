package com.opp.OrganizacijaFestivala.model;

import com.opp.OrganizacijaFestivala.rest.dto.OrgAppDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class OrganizationApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private boolean confirmed = false;

    @NotNull
    private Long organizerId;

    @NotNull
    @ManyToOne
    private Festival festival;

    public OrganizationApplication(){
        super();
    }

    public OrganizationApplication(Long organizerId, Festival festival){
        this.organizerId = organizerId;
        this.festival = festival;
    }


    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public long getId() {
        return id;
    }

    public Long getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(Long organizerId) {
        this.organizerId = organizerId;
    }
}
