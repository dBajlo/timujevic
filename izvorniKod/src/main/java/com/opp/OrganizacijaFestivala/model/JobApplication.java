package com.opp.OrganizacijaFestivala.model;

import com.opp.OrganizacijaFestivala.rest.dto.JobAppDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
public class JobApplication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @ManyToOne
    RegisteredUser performer;

    @NotNull
    Long jobId;

    @NotNull
    int price;

    String comment;

    @NotNull
    int numWorkers;
    
    int numPrinted;

    @NotNull
    int jobDuration;

    boolean confirmed = false;

    public JobApplication(){
        super();
    }

    public JobApplication(JobAppDTO dto, RegisteredUser performer){
        this.performer = performer;
        this.jobId  = dto.getJobId();
        price = dto.getPrice();
        comment = dto.getComment();
        numWorkers = dto.getNumWorkers();
        jobDuration = dto.getJobDuration();
        numPrinted=0;

    }
    
    
    public int getNumPrinted()
    {
    	return this.numPrinted;
    }
    
    public void setNumPrinted(int numPrint)
    {
    	this.numPrinted=numPrint;
    }
    

    public long getId() {
        return id;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public RegisteredUser getPerformer() {
        return performer;
    }

    public void setPerformer(RegisteredUser performer) {
        this.performer = performer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getNumWorkers() {
        return numWorkers;
    }

    public void setNumWorkers(int numWorkers) {
        this.numWorkers = numWorkers;
    }

    public int getJobDuration() {
        return jobDuration;
    }

    public void setJobDuration(int jobDuration) {
        this.jobDuration = jobDuration;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }
}
