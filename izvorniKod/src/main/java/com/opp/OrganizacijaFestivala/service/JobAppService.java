package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.JobApplication;
import com.opp.OrganizacijaFestivala.rest.dto.JobAppDTO;
import com.opp.OrganizacijaFestivala.rest.dto.JobAppDTO2;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface JobAppService {
    List<JobApplication> findAll();
    List<JobApplication> findAllByJobId(Long jobId);
    List<JobApplication> findConfirmed(Long jobId);
    List<JobApplication> findUnconfirmed(Long jobId);
    List<JobApplication> findUserJobApp(Long userId);
    void delete(Long id);
    void delete(JobApplication jobApplication);
    boolean exists(Long id);
    JobApplication createJobApp(JobAppDTO dto);
    boolean confirm(Long id);
    
    //@Transactional
    //@Modifying(clearAutomatically = true, flushAutomatically = true)
    //@Query("UPDATE JobApplication j SET j.numPrinted=j.numPrinted+1 WHERE j.id=id")
    //public int updateNumPrinted(Long id);
    

    JobApplication findById(Long id);

    List<JobAppDTO2> findUserJobApp2(Long id);
}
