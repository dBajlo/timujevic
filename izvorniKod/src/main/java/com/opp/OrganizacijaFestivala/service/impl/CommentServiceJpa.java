package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.Comment;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.repository.CommentRepository;
import com.opp.OrganizacijaFestivala.repository.UserRepository;
import com.opp.OrganizacijaFestivala.rest.dto.CommentDTO;
import com.opp.OrganizacijaFestivala.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;


import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceJpa implements CommentService {
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    UserRepository userRepository;

    @Override
    public List<Comment> getProfileComments(Long id) {
        Optional<RegisteredUser> profile = userRepository.findById(id);
        Assert.isTrue(profile.isPresent(), "Ne postoji profil na kojem se pokušava pregledati komentare");
        return commentRepository.findAllByProfile(profile.get());
    }

    @Override
    public List<Comment> getCommenterComments(Long id) {
        Optional<RegisteredUser> commenter = userRepository.findById(id);
        Assert.isTrue(commenter.isPresent(), "Ne postoji komentator čiji komentari se pokušavaju pregledati");
        return commentRepository.findAllByProfile(commenter.get());
    }

    @Override
    public Comment create(CommentDTO commentDTO) {
        Optional<RegisteredUser> profile = userRepository.findById(commentDTO.getProfileId());
        Assert.isTrue(profile.isPresent(), "Ne postoji profil na kojem se pokušava ostaviti komentar");
        Assert.isTrue(profile.get().getRole().equals("izvodjac"), "Profil "+
                profile.get().getUsername()+" ne pripada izvođaču, već "+profile.get().getRole()+"u");
        Optional<RegisteredUser> commenter = userRepository.findById(commentDTO.getCommenterId());
        Assert.isTrue(commenter.isPresent(), "Ne postoji komentator koji pokušava ostaviti komentar");
        Assert.isTrue(commenter.get().getRole().equals("organizator"), "Korisnik "+
                commenter.get().getUsername()+" nije organizator već "+commenter.get().getRole());
        //napraviti provjeru je li obavio posao za njega
        //napraviti provjeru postoji li posao
        Assert.isTrue(commentDTO.getComment().trim().length()>0, "Sadržaj komentara mora biti zadan");
        Comment comment = new Comment(profile.get(), commenter.get(), commentDTO.getComment().trim(),
                commentDTO.getJobId());
        return commentRepository.save(comment);
    }

    @Override
    public void delete(Comment comment) {
        commentRepository.delete(comment);
    }

    @Override
    public void delete(Long commentId) {
        commentRepository.deleteById(commentId);
    }
}
