package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.*;
import com.opp.OrganizacijaFestivala.repository.*;
import com.opp.OrganizacijaFestivala.rest.dto.JobAppDTO;
import com.opp.OrganizacijaFestivala.rest.dto.JobAppDTO2;
import com.opp.OrganizacijaFestivala.service.JobAppService;
import com.opp.OrganizacijaFestivala.service.JobService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JobAppServiceJpa implements JobAppService {
    @Autowired
    JobApplicationRepository repository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JobRepository jobRepository;

    @Autowired
    JobService jobService;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    FestivalRepository festivalRepository;

    @Override
    public List<JobApplication> findAll() {
        return repository.findAll();
    }

    @Override
    public List<JobApplication> findAllByJobId(Long jobId) {
        return repository.findAllByJobId(jobId);
    }

    @Override
    public List<JobApplication> findUnconfirmed(Long jobId) {
        return repository.findAllByConfirmedAndJobId(false, jobId);
    }

    @Override
    public List<JobApplication> findConfirmed(Long jobId) {
        return repository.findAllByConfirmedAndJobId(true, jobId);
    }

    @Override
    public List<JobApplication> findUserJobApp(Long userId) {
        Optional<RegisteredUser> performer = userRepository.findById(userId);
        Assert.isTrue(performer.isPresent(), "Ne postoji izvođač čije prijave se traže");
        Assert.isTrue(performer.get().getRole().equals("izvodjac"), "Korisnik čije prijave " +
                "za licitaciju se traže nije izvođač, već "+performer.get().getRole());
        return repository.findAllByPerformer(performer.get());
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void delete(JobApplication jobApplication) {
        repository.delete(jobApplication);
    }


    @Override
    public boolean exists(Long id) {
        return repository.existsById(id);
    }

    @Override
    public JobApplication createJobApp(JobAppDTO dto) {
        RegisteredUser performer = userRepository.findByUsername(dto.getPerformer());
        System.out.println(dto.getJobId());
        Optional<Job> job = jobRepository.findById(dto.getJobId());
        Assert.isTrue(job.isPresent(), "Posao mora postojati");
        Job job1 = jobService.checkAuctionOver(job.get());
        Assert.isTrue(job1.isAuctionOpen(), "Ne može se prijaviti na zatvorenu licitaciju");
        JobApplication japp = new JobApplication(dto, performer);

        return repository.save(japp);
    }

    @Override
    public boolean confirm(Long id) {
        Optional<JobApplication> jobApplication = repository.findById(id);
        Assert.isTrue(jobApplication.isPresent(), "Prijava koju se pokušava potvrditi ne postoji.");
        jobApplication.get().setConfirmed(true);
        repository.save(jobApplication.get());
        return true;
    }

    @Override
    public JobApplication findById(Long id) {
        Optional<JobApplication> jobApplication = repository.findById(id);
        Assert.isTrue(jobApplication.isPresent(), "Ne postoji ta prijava za licitaciju");
        return jobApplication.get();
    }

    @Override
    public List<JobAppDTO2> findUserJobApp2(Long id) {
        Optional<RegisteredUser> performer = userRepository.findById(id);
        Assert.isTrue(performer.isPresent(), "Ne postoji izvođač čije prijave se traže");
        Assert.isTrue(performer.get().getRole().equals("izvodjac"), "Korisnik čije prijave " +
                "za licitaciju se traže nije izvođač, već "+performer.get().getRole());
        List<JobApplication> jobApplications = repository.findAllByPerformer(performer.get());
        List<JobAppDTO2> jobAppDTO2s = new ArrayList<>();
        for(JobApplication jobApplication: jobApplications){
            Optional<Job> job = jobRepository.findById(jobApplication.getJobId());
            Assert.isTrue(job.isPresent(), "Posao kojem pripada ta prijava ne postoji");
            Optional<Event> event = eventRepository.findById(job.get().getEventId());
            Assert.isTrue(event.isPresent(), "Događanje kojem pripada ta prijava ne postoji");
            Optional<Festival> festival = festivalRepository.findById(event.get().getFestivalId());
            Assert.isTrue(festival.isPresent(), "Festival kojem pripada ta prijava ne postoji");
            String performerName = "";

            if(job.get().getWinAppId()!=null){
                try {
                    Optional<JobApplication> win = repository.findById(job.get().getWinAppId());
                    if(win.isPresent()){
                        performerName = win.get().getPerformer().getUsername();
                    }

                } catch( NullPointerException e){
                    System.err.println("Greška u pronalaženju pobjedničkog izvođača");
                }
            }
            jobAppDTO2s.add(new JobAppDTO2(jobApplication, event.get().getEventName(), festival.get().getFestivalName(),
                    job.get().getOrdinalNumber(), festival.get().getEndTime().isBefore(LocalDateTime.now()),
                    performerName, event.get().getOrganizer().getUsername(), job.get().getJobId()));
        }
        return jobAppDTO2s;
    }
    
    
}
