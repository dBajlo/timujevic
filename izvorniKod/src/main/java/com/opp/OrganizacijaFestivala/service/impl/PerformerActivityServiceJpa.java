package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.Performer_Activity;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.repository.ActivityRepository;
import com.opp.OrganizacijaFestivala.repository.PerformerActivityRepo;
import com.opp.OrganizacijaFestivala.service.PerformerActivityService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PerformerActivityServiceJpa implements PerformerActivityService {
    @Autowired
    private PerformerActivityRepo performerActivityRepo;

    @Autowired
    private ActivityRepository activityRepository;

    @Override
    public List<Activity> findAllByPerformerId(Long id) {
        List<Performer_Activity> performer_activities =  performerActivityRepo.findAllByPerformerId(id);
        List<Activity> activities = new ArrayList<>();
        for(Performer_Activity p: performer_activities){
            activities.add(activityRepository.findByActivityId(p.getActivityId()));
        }
        return activities;
    }

    @Override
    public  List<Performer_Activity> findAllByActivityId(Long id) {
        return performerActivityRepo.findAllByActivityId(id);
    }

    @Override
    public  List<Performer_Activity> findAllByPerformerIdAndActivityId(Long pId, Long aId) {
        return performerActivityRepo.findAllByPerformerIdAndActivityId(pId, aId);

    }

    @Override
    public boolean delete(Long pId, Long aId) {
        List<Performer_Activity> performer_activity = performerActivityRepo.findAllByPerformerIdAndActivityId(pId, aId);
        if(performer_activity.isEmpty()) return true;
        for(Performer_Activity p: performer_activity){
            performerActivityRepo.delete(p);
        }
        return true;
    }

    @Override
    public boolean delete(Performer_Activity performer_activity) {
        performerActivityRepo.delete(performer_activity);
        return true;
    }

    @Override
    public Performer_Activity create(Long idP, Long idA) {
        Performer_Activity p = new Performer_Activity(idP, idA);
        List<Performer_Activity> performer_activity = performerActivityRepo.findAllByPerformerIdAndActivityId(idP, idA);
        Assert.isTrue(performer_activity.isEmpty(), "Već je dodana ta djelatnost");
        return performerActivityRepo.save(p);
    }
}
