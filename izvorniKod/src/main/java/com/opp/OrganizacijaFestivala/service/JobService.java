package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.rest.dto.JobActivityDTO;
import com.opp.OrganizacijaFestivala.rest.dto.JobDTO;
import com.opp.OrganizacijaFestivala.rest.dto.JobDTO2;

import java.util.List;

public interface JobService {
    Job createJob (JobDTO jobDTO);
    Job openAuction(Long id);
    Job closeAuction(Long id);
    Job closeAuction(Job job);
    List<Job> getOpenAuctions();
    List<Job> eventJobs(Long id);
    public List<JobActivityDTO> getJobsAndActivities(Long id);
    void delete(Long id);
    Job getJobById(Long id);
    Job checkAuctionOver(Job job);
    List<JobDTO2> getOpenAuctions2();
    public List<JobDTO2> eventJobs2(Long id);
}
