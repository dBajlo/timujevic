package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.exception.EmailExistsException;
import com.opp.OrganizacijaFestivala.exception.InvalidRoleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.json.JSONObject;
import com.opp.OrganizacijaFestivala.rest.dto.ActivityDTO;
import com.opp.OrganizacijaFestivala.service.ActivityService;

import java.util.List;

@RestController
@RequestMapping("/activities")
public class ActivityController 
{
	
	@Autowired
    private ActivityService activityService;

	@PostMapping("/create")
    public Activity createActivity(@RequestParam String name) {
        return  activityService.createActivity(name);
    }

	
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Secured("ROLE_ADMIN")
    public void deleteActivity(@PathVariable(value = "id") Long id){
        activityService.deleteById(id);
    }

    @GetMapping("/listActivities")
    public List<Activity> findAllActivities(){
        return activityService.findAllActivities();
    }
}
