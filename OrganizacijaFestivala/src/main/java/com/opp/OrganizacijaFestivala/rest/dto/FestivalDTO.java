package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.Festival;
import com.sun.istack.NotNull;

import java.time.LocalDateTime;
import java.util.Date;

public class FestivalDTO {
    @NotNull

    private String leader;

    @NotNull
    private String festivalName;

    private String description;
    @NotNull
    private LocalDateTime startTime;
    @NotNull
    private LocalDateTime endTime;

    private Byte[] logo;

    public FestivalDTO(){
        super();
    }

    public FestivalDTO(Festival festival){
        leader = festival.getLeader().getUsername();
        festivalName = festival.getFestivalName();
        description = festival.getDescription();
        startTime = festival.getStartTime();
        endTime = festival.getEndTime();
        logo = festival.getLogo();
    }

    public FestivalDTO(String leader, String festivalName, String description, LocalDateTime startTime, LocalDateTime endTime) {
        this.leader = leader;
        this.festivalName = festivalName;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Byte[] getLogo() {
        return logo;
    }

    public void setLogo(Byte[] logo) {
        this.logo = logo;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
