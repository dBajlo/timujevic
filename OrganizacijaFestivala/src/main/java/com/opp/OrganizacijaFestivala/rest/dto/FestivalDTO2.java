package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;

import java.time.LocalDateTime;

public class FestivalDTO2 {

    private Festival festival;

    private boolean finished;

    public FestivalDTO2(){
        super();
    }

    public FestivalDTO2(Festival festival, boolean finished){
        this.festival = festival;
        this.finished = finished;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
