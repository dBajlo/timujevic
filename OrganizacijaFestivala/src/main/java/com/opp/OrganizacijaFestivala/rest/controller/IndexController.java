package com.opp.OrganizacijaFestivala.rest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @GetMapping("/")
    public String index() {
        return "Aplikaciju za Organizaciju Festivala\n" +
                "\nNapravlili:" +
                "\n\tDanijel Bajlo," +
                "\n\tEmanuel Evačić," +
                "\n\tFran Jelenić," +
                "\n\tIvan Radiković," +
                "\n\tLovre Petešić," +
                "\n\tMartin Modrušan i " +
                "\n\tPetar Jerković.";
    }
}