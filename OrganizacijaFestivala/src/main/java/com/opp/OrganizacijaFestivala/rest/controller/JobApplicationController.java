package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.model.JobApplication;
import com.opp.OrganizacijaFestivala.repository.JobApplicationRepository;
import com.opp.OrganizacijaFestivala.rest.dto.JobAppDTO;
import com.opp.OrganizacijaFestivala.rest.dto.JobAppDTO2;
import com.opp.OrganizacijaFestivala.service.JobAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/jobApps")
public class JobApplicationController {
    @Autowired
    private JobAppService jobAppService;
    
    @Autowired
    private JobApplicationRepository jobAppRepository;


    @PostMapping("/create")
    public JobApplication createApp(@RequestBody JobAppDTO jobAppDTO){

        return jobAppService.createJobApp(jobAppDTO);
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public JobApplication getById(@PathVariable(value = "id") Long id){
        return jobAppService.findById(id);
    }

    @GetMapping("/listApps")
    public List<JobApplication> listApps(){
        return jobAppService.findAll();
    }

    @GetMapping(value = "listConfirmed/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobApplication> listConfirmedApps(@PathVariable(value = "id") Long jobId){
        return jobAppService.findConfirmed(jobId);
    }

    @GetMapping(value = "listUnconfirmed/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobApplication> listUnconfirmedApps(@PathVariable(value = "id") Long jobId){
        return jobAppService.findUnconfirmed(jobId);
    }

    @GetMapping("/userApps")
    public List<JobApplication> listUserApps(@RequestParam Long id){
        return jobAppService.findUserJobApp(id);
    }

    @GetMapping("/userApps2")
    public List<JobAppDTO2> listUserApps2(@RequestParam Long id){
        return jobAppService.findUserJobApp2(id);
    }


    @GetMapping("/jobApps")
    public List<JobApplication> listJobApps(@RequestParam Long id){
        return jobAppService.findAllByJobId(id);
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteApp(@PathVariable(value = "id") Long id){
        jobAppService.delete(id);
    }

    @PutMapping(value = "confirm/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean confirmApp(@PathVariable(value = "id") Long id){
        return jobAppService.confirm(id);
    }

}
