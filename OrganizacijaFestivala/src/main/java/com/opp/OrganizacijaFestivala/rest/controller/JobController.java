package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.model.Job_Activity;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.rest.dto.*;
import com.opp.OrganizacijaFestivala.service.JobActivityService;
import com.opp.OrganizacijaFestivala.service.JobService;
import com.opp.OrganizacijaFestivala.service.UserService;
import com.opp.OrganizacijaFestivala.exception.EmailExistsException;
import com.opp.OrganizacijaFestivala.exception.InvalidRoleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.json.JSONObject;

import java.util.List;

@RestController
@RequestMapping("/jobs")
public class JobController {
    @Autowired
    private JobService jobService;

    @Autowired
    private JobActivityService jobActivityService;


    @PostMapping("/create")
    public Job createJob(@RequestBody JobDTO JobDTO){

        return jobService.createJob(JobDTO);
    }

    @PutMapping(value = "openAuction/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Job openAucation(@PathVariable(value = "id") Long id){
        return jobService.openAuction(id);
    }

    @PutMapping(value = "closeAuction/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Job closeAucation(@PathVariable(value = "id") Long id){
        return jobService.closeAuction(id);
    }


    @GetMapping("/auctions")
    public List<Job> listAuctions(){
        return jobService.getOpenAuctions();
    }


    //DODAVANJE POSTOJEĆE DJELATNOSTI POSLU
    @PostMapping("/activities")
    @Secured("ROLE_ORGANIZER")
    public Job_Activity addActivity(@RequestParam Long jId, @RequestParam Long aId){
        return jobActivityService.create(jId, aId);
    }

    //DOHVAĆANJE POSLA
    @GetMapping(value = "{id}")
    public Job getJobWithId(@PathVariable(value = "id") Long id){
        return jobService.getJobById(id);
    }

    //DOHVAĆANJE DJELATNOSTI NEKOG POSLA
    @GetMapping(value = "activities/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Activity> getActivities(@PathVariable Long id) {
        return jobActivityService.findAllByJobId(id);
    }

    //BRISANJE POSLA
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long deleteJob(@PathVariable Long id){
        jobService.delete(id);
        return id;
    }


    //MICANJE DJELATNOSTI S PROFILA POSLA
    @DeleteMapping("/activities")
    public void removeActivity(@RequestParam Long jId, @RequestParam Long aId){
        jobActivityService.delete(jId, aId);
    }

    @GetMapping("/getJobsAndActivitiesOpen")
    //prenosi aktivnosti kao string za lakši ispis u frontendu
    public List<JobDTO2> getJobDTO2(){
        return jobService.getOpenAuctions2();
    }

    @GetMapping(value = "getJobsAndActivities/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobDTO2> getJobsForEvent(@PathVariable Long id){
        return jobService.eventJobs2(id);
    }
}
