package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.model.OrganizationApplication;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.repository.FestivalRepository;
import com.opp.OrganizacijaFestivala.repository.OrganizationApplicationRepository;
import com.opp.OrganizacijaFestivala.repository.UserRepository;
import com.opp.OrganizacijaFestivala.rest.dto.OrgAppDTO;
import com.opp.OrganizacijaFestivala.rest.dto.OrgAppDTO2;
import com.opp.OrganizacijaFestivala.service.OrgAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class OrgAppServiceJpa implements OrgAppService {
    @Autowired
    OrganizationApplicationRepository repository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    FestivalRepository festivalRepository;

    @Override
    public List<OrganizationApplication> findAll() {
        return repository.findAll();
    }

    @Override
    public List<RegisteredUser> findConfirmed(Long id) {
        List<OrganizationApplication> orgList = repository.findAllByConfirmedAndFestivalId(true, id);
        List<RegisteredUser> userList = new ArrayList<>();
        Optional<RegisteredUser> user;
        for(OrganizationApplication o: orgList){
            user = userRepository.findById(o.getOrganizerId());
            if(user.isPresent()){
                userList.add(user.get());
            }
        }
        Assert.isTrue(!userList.isEmpty(), "Nema organizatora čija prijava za ovaj festival je potvrđena.\n" +
                "Bez organizatora ne može se stvoriti događanje.");
        return userList;
    }

    @Override
    public List<OrgAppDTO2> organizersUnconfirmedApps(Long organizerId) {
        List<OrganizationApplication> applications = repository.findAllByOrganizerIdAndConfirmed(organizerId, false);
        List<OrgAppDTO2> orgAppList = new ArrayList<>();
        for (OrganizationApplication application : applications) {
            orgAppList.add(new OrgAppDTO2(
                            application,
                            userRepository
                                    .findById(organizerId).orElseThrow(() -> new EntityNotFoundException("Not found: " + organizerId))
                                    .getUsername()));
        }
        return orgAppList;
    }

    @Override
    public List<OrgAppDTO2> findUnconfirmed(Long id) {
        List<OrganizationApplication> orgList = repository.findAllByConfirmedAndFestivalId(false, id);
        List<OrgAppDTO2> dtoList = new ArrayList<>();
        Optional<RegisteredUser> user;
        for(OrganizationApplication o: orgList){
            user = userRepository.findById(o.getOrganizerId());
            if(user.isPresent()){
                dtoList.add(new OrgAppDTO2(o, user.get().getUsername()));
            }
        }
        return dtoList;
    }

    @Override
    public List<OrganizationApplication> findUserOrgApp(Long userId) {
        return repository.findAllByOrganizerId(userId);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public boolean exists(Long id) {
        return repository.existsById(id);
    }

    @Override
    public OrganizationApplication createOrgApp(OrgAppDTO dto) {
        LocalDateTime dateTime = LocalDateTime.now();
        RegisteredUser organizer = userRepository.findByUsername(dto.getOrganizer());
        Assert.notNull(organizer, "Organizator koji stvara prijavu ne postoji");
        Assert.isTrue(organizer.getRole().equals("organizator"), "Korisnik mora biti organizator da bi se prijavio.");
        Optional<Festival> festival = festivalRepository.findById(dto.getFestivalId());
        Assert.isTrue(festival.isPresent(), "Festival za koji se prijavljuje ne postoji");
        Assert.isTrue(!repository.existsByOrganizerIdAndFestivalId(organizer.getId(), festival.get().getId())
        , "Već ste se prijavili za organizaciju tog festivala");
        Assert.isTrue(festival.get().getEndTime().isAfter(dateTime), "Ne može se prijaviti na festival koji je završio");
        OrganizationApplication application = new OrganizationApplication(organizer.getId(), festival.get());
        repository.save(application);
        return application;
    }

    @Override
    public OrganizationApplication confirm(Long id) {
        Optional<OrganizationApplication> orgApplication = repository.findById(id);
        Assert.isTrue(orgApplication.isPresent(), "Prijava koju se pokušava potvrditi ne postoji.");
        orgApplication.get().setConfirmed(true);
        repository.save(orgApplication.get());
        return orgApplication.get();
    }
}
