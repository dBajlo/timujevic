package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.model.Job_Activity;
import com.opp.OrganizacijaFestivala.repository.ActivityRepository;
import com.opp.OrganizacijaFestivala.repository.JobActivityRepo;
import com.opp.OrganizacijaFestivala.repository.JobRepository;
import com.opp.OrganizacijaFestivala.rest.dto.JobActivityDTO;
import com.opp.OrganizacijaFestivala.service.JobActivityService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JobActivityServiceJpa implements JobActivityService {
    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private JobActivityRepo jobActivityRepo;

    @Autowired
    private JobRepository jobRepository;

    @Override
    public List<Activity> findAllByJobId(Long id) {
        List<Job_Activity> job_activities = jobActivityRepo.findAllByJobId(id);
        List<Activity> activities = new ArrayList<>();
        Activity activity;
        for(Job_Activity j: job_activities){
            activity = activityRepository.findByActivityId(j.getActivityId());
            Assert.notNull(activity, "Posao ima aktivnost koja ne postoji u bazi");
            activities.add(activity);
        }
        return activities;
    }

    @Override
    public boolean delete(Long jId, Long aId) {
        List<Job_Activity> allActivities = jobActivityRepo.findAllByJobId(jId);
        Assert.isTrue(allActivities.size()>1, "Ne može se obrisati posljednja djelatnost");
        List<Job_Activity> job_activity = jobActivityRepo.findAllByJobIdAndActivityId(jId, aId);
        if(job_activity.isEmpty()) return true;
        for(Job_Activity p: job_activity){
            jobActivityRepo.delete(p);
        }
        return true;
    }

    @Override
    public Job_Activity create(Long jId, Long aId) {
        Job_Activity j = new Job_Activity(jId, aId);
        List<Job_Activity> job_activity = jobActivityRepo.findAllByJobIdAndActivityId(jId, aId);
        Assert.isTrue(job_activity.isEmpty(), "Već je dodana ta djelatnost");
        return jobActivityRepo.save(j);
    }

    @Override
    public boolean delete(Job_Activity job_activity) {
        jobActivityRepo.delete(job_activity);
        return true;
    }


}
