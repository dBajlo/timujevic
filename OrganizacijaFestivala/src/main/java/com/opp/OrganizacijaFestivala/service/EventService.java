package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Event;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.rest.dto.EventDTO;

import java.util.List;

public interface EventService {

    List<Event> findAll();
    List<Event> findAllByOrganizer(RegisteredUser organizer);
    void delete(Long id);
    boolean exists(Long id);
    Event createEvent(EventDTO eventDto);
    Event findById(Long id);
    List<Event> festivalEvents(Long id);

    List<Event> findAllByOrganizerAndIsActive(RegisteredUser organizer, boolean isActive);
}
