package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Activity;

import com.opp.OrganizacijaFestivala.rest.dto.ActivityDTO;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


public interface ActivityService 
{
	
	List<Activity> findAllActivities();
	Activity findByActivityName(String activity_name);
	Activity findByActivityId(Long activity_id);
	boolean existsById(Long id);
	void deleteById(Long id);

	Activity createActivity(String name);
}
