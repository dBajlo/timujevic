package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.Performer_Activity;
import com.opp.OrganizacijaFestivala.repository.UserRepository;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.exception.CustomException;
import com.opp.OrganizacijaFestivala.exception.EmailExistsException;
import com.opp.OrganizacijaFestivala.rest.dto.UserDTO;
import com.opp.OrganizacijaFestivala.security.JwtTokenProvider;
import com.opp.OrganizacijaFestivala.security.MyUserDetailsService;
import com.opp.OrganizacijaFestivala.service.PerformerActivityService;
import com.opp.OrganizacijaFestivala.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class UserServiceJpa implements UserService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PerformerActivityService performerActivityService;

    public String login(String username, String password) {

        try {
            //System.out.println("usli smo u jps try login");
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            //System.out.println("Stvaramo token za: "+username + MyUserDetailsService.getAuthorities(username,
              //      repository.findByUsername(username).getRole()));
            return jwtTokenProvider.createToken(username, MyUserDetailsService.getAuthorities(username,
                    repository.findByUsername(username).getRole()));
        } catch (AuthenticationException e) {
            //System.out.println("uhvacen authexception");
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void delete(String username) {
        repository.deleteByUsername(username);
    }

    public RegisteredUser whoami(HttpServletRequest req) {
        return repository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
    }

    @Override
    public RegisteredUser currentUser(String token) {
        return repository.findByUsername(jwtTokenProvider.getUsername(token));
    }

    @Override
    public RegisteredUser findById(Long id) {
        return repository.findById(id).get();
    }



    public String refresh(String username) {
        List<GrantedAuthority> authorities = repository.findByUsername(username).getRoleAuthorities();
        return jwtTokenProvider.createToken(username, authorities);
    }

    @Override
    public List<RegisteredUser> findAll() {
        return repository.findAll();
    }

    @Override
    public List<RegisteredUser> findAllByRole(String role) {
        Assert.notNull(role, "Role must not be null.");
        List<RegisteredUser> lista = repository.findAllByRole(role);
        return lista;
    }

    @Override
    public RegisteredUser findByUsername(String username) {
        RegisteredUser user = repository.findByUsername(username);
        if (user == null) {
            throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
        }
        return user;
    }

    @Transactional
    @Override
    public String registerNewUserAccount(UserDTO accountDto)
    {
        String email = accountDto.getEmail();
        final RegisteredUser user = new RegisteredUser();
        String token = null;
        String username = accountDto.getUsername();

        Assert.isTrue(accountDto.getFirst_name().length()>0, "Ime mora biti zadano");
        Assert.isTrue(accountDto.getUsername().length()>0, "Korisničko ime mora biti zadano");
        Assert.isTrue(accountDto.getPassword().length()>0, "Lozinka mora biti zadana");
        Assert.isTrue(isValid(email), "Format e-mail adrese je neispravan!");
        Assert.isTrue(!emailExist(email), "E-mail adresa "+email+" je već zauzeta!");
        Assert.isTrue(!repository.existsByUsername(accountDto.getUsername()),
                "Korisničko ime "+username+" je već zauzeto!");

        user.setFirst_name(accountDto.getFirst_name());
        user.setLast_name(accountDto.getLast_name());
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setEmail(accountDto.getEmail());
        user.setUsername(accountDto.getUsername());
        user.setRole(accountDto.getRole());
        user.setPhone_number(accountDto.getPhone_number());
        user.setProfilePicture(accountDto.getProfilePicture());
        repository.save(user);
        token = jwtTokenProvider.createToken(user.getUsername(), user.getRoleAuthorities());



        return  "{\"token\": \""+token+"\"}";
    }

    private static boolean isValid(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    @Override
    public RegisteredUser updateUser(Long id) {
        Optional<RegisteredUser> existing = repository.findById(id);
        if(existing == null){
            System.err.println("Ne postoji korisnik s identifikacijskim brojem "+id);
            return null;
        }
        if(existing.get().getRole().equals("nepotvrdjeni_voditelj")){
            existing.get().setRole("voditelj");
            return repository.save(existing.get());
        }
        else {
            System.err.println("Dani korisnik nije nepotvrđeni voditelj, nego "+existing.get().getRole());
            return null;
        }

    }

    @Override
    public void delete(Long id) {
        RegisteredUser admin = repository.findByUsername("admin");

        if(admin.getId() == id){
            throw new CustomException("Ne može se obrisati admin!", HttpStatus.BAD_REQUEST);
        }
        else {
            repository.deleteById(id);
        }
        return;
    }

    private boolean emailExist(String email) {
        RegisteredUser user = repository.findByEmail(email);
        if (user != null) {
            return true;
        }
        return false;
    }


}
