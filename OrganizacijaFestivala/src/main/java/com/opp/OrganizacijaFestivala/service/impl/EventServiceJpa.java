package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.Event;
import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.repository.EventRepository;
import com.opp.OrganizacijaFestivala.repository.FestivalRepository;
import com.opp.OrganizacijaFestivala.repository.JobRepository;
import com.opp.OrganizacijaFestivala.repository.UserRepository;
import com.opp.OrganizacijaFestivala.rest.dto.EventDTO;
import com.opp.OrganizacijaFestivala.service.EventService;
import com.opp.OrganizacijaFestivala.service.JobService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class EventServiceJpa implements EventService {
    @Autowired
    EventRepository eventRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    FestivalRepository festivalRepository;
    @Autowired
    JobRepository jobRepository;
    @Autowired
    JobService jobService;


    @Override
    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    @Override
    public List<Event> findAllByOrganizer(RegisteredUser organizer) {
        return eventRepository.findAllByOrganizer(organizer);
    }

    @Override
    public void delete(Long id) {
        List<Job> jobs = jobRepository.findAllByEventId(id);
        for(Job job: jobs){
            jobService.delete(job.getJobId());
        }
        eventRepository.deleteById(id);
    }

    @Override
    public boolean exists(Long id) {
        return eventRepository.existsById(id);
    }

    @Override
    public Event createEvent(EventDTO eventDto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Assert.isTrue(eventDto.getEventName().length()>0, "Ime događanja mora biti zadano");
        Assert.notNull(eventDto.getStartTime(), "Vrijeme početka mora biti zadano");
        Assert.notNull(eventDto.getEndTime(), "Vrijeme završetka mora biti zadano");
        LocalDateTime dateTime = LocalDateTime.now();
        Assert.isTrue(eventDto.getStartTime().isAfter(dateTime), "Ne može se stvoriti događanje koji je već počelo");
        Assert.isTrue(eventDto.getStartTime().isBefore(eventDto.getEndTime()),
                "Vrijeme završetka mora biti nakon vremena početka");
        RegisteredUser organizer = userRepository.findByUsername(eventDto.getOrganizer());
        Assert.notNull(organizer, "Organizator tog imena ne postoji");
        Optional<Festival> festival = festivalRepository.findById(eventDto.getFestivalId());
        Assert.isTrue(festival.isPresent(), "Festival za koji se stvara događanje mora postojati");
        Assert.isTrue(festival.get().getStartTime().isBefore(eventDto.getStartTime()),
                "Događanje ne može započeti prije festivala");
        Assert.isTrue(festival.get().getEndTime().isAfter(eventDto.getEndTime()),
                "Događanje ne može trajati dulje od festivala");


        if(eventDto.getEventDescription()==null || eventDto.getEventDescription().trim().length()==0){
            eventDto.setEventDescription("nema opisa");
        }

        List<Event> organizerEvents = eventRepository.findAllByOrganizer(organizer);
        for(Event e: organizerEvents){
            Assert.isTrue(eventDto.getStartTime().isAfter(e.getEndTime())||
                    eventDto.getEndTime().isBefore(e.getStartTime()),
                    "Organizator "+organizer.getUsername()+" već je zauzet u terminu od " +
                    e.getStartTime().format(formatter) + " do " + e.getEndTime().format(formatter));
        }

        Event event = new Event(eventDto, organizer);
        return eventRepository.save(event);
    }

    @Override
    public Event findById(Long id) {
        Optional<Event> event = eventRepository.findById(id);
        Assert.isTrue(event.isPresent(), "Ne postoji to događanje");
        return event.get();
    }

    @Override
    public List<Event> festivalEvents(Long id) {
        return eventRepository.findAllByFestivalId(id);
    }

    @Override
    public List<Event> findAllByOrganizerAndIsActive(RegisteredUser organizer, boolean isActive) {
        if(isActive){
            return eventRepository.findAllByOrganizerAndEndTimeAfter(organizer, LocalDateTime.now());
        }

        else{
            return eventRepository.findAllByOrganizerAndEndTimeBefore(organizer, LocalDateTime.now());
        }
    }

}
