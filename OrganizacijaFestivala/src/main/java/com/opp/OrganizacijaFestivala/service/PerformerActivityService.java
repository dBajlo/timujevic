package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.Performer_Activity;

import java.util.List;

public interface PerformerActivityService {
    List<Activity> findAllByPerformerId(Long id);
    List<Performer_Activity> findAllByActivityId(Long id);
    List<Performer_Activity> findAllByPerformerIdAndActivityId(Long pId, Long aId);
    boolean delete(Long pId, Long aId);
    boolean delete(Performer_Activity performer_activity);
    Performer_Activity create(Long idP, Long idA);
}
