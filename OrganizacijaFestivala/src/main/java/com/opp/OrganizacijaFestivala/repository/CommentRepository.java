package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.Comment;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllByProfile(RegisteredUser profile);
    List<Comment> findAllByCommenter(RegisteredUser commenter);
}
