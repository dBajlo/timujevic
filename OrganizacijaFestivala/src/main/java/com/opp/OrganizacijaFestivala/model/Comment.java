package com.opp.OrganizacijaFestivala.model;

import com.opp.OrganizacijaFestivala.rest.dto.CommentDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    private RegisteredUser profile;

    @NotNull
    @ManyToOne
    private RegisteredUser commenter;

    @NotNull
    private LocalDateTime time;

    @NotNull
    private String comment;

    //@NotNull
    @NotNull
    private Long jobId;

    public Comment(){
        super();
    }

    public  Comment(RegisteredUser profile, RegisteredUser commenter, String comment, Long jobId){
        time = LocalDateTime.now();
        this.profile = profile;
        this.comment = comment;
        this.commenter = commenter;
        this.jobId = jobId;
        //dodat posao
    }

    public Long getId() {
        return id;
    }

    public RegisteredUser getProfile() {
        return profile;
    }

    public void setProfile(RegisteredUser profile) {
        this.profile = profile;
    }

    public RegisteredUser getCommenter() {
        return commenter;
    }

    public void setCommenter(RegisteredUser commenter) {
        this.commenter = commenter;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }
}
