package com.opp.OrganizacijaFestivala.model;

import com.opp.OrganizacijaFestivala.rest.dto.ActivityDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long activityId;
    @NotNull
    @Column(unique = true)
    private String activityName;

    public Activity(){
        super();
    }

    public Activity(String name) {
        activityName = name;
    }

    public long getActivityId() {
        return activityId;
    }


    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
}