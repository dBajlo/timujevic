package com.opp.OrganizacijaFestivala.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * VAZNO!!
 * Da bi pokrenuo testove moras instalirati webdriver i njegov path zalijepiti na liniju 24 u System.setProperty()
 */
public class SeleniumTests {

    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://organizacijafestivala.herokuapp.com/login.html");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void prijavaKorisnikaTest() throws InterruptedException {
        String imeKorisnika = "er";
        String pass = "er";

        driver.findElement(By.id("korisnickoIme")).click();
        driver.findElement(By.id("korisnickoIme")).sendKeys(imeKorisnika);
        driver.findElement(By.id("password")).sendKeys(pass);
        Thread.sleep(2000);
        driver.findElement(By.id("loginButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("myProfile")).click();
        Thread.sleep(2000);
        assertEquals("Korisnik " + imeKorisnika, driver.findElement(By.id("username")).getText());
    }

    @Test
    public void stvaranjeFestivalaTest() throws InterruptedException {
        String imeFestivala = "festivalAlpha" + (int) (Math.random() * 1000);
        String imeVoditelja = "er";
        String pass = "er";

        driver.findElement(By.id("korisnickoIme")).click();
        driver.findElement(By.id("korisnickoIme")).sendKeys(imeVoditelja);
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys(pass);
        Thread.sleep(1000);
        driver.findElement(By.id("loginButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("myProfile")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("festButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("ime")).click();
        driver.findElement(By.id("ime")).sendKeys(imeFestivala);
        driver.findElement(By.id("opis")).sendKeys("opis");
        driver.findElement(By.id("vrijemePoc")).sendKeys("277\t2020\t0315"); // 27 Jul 2020, 03:15
        driver.findElement(By.id("vrijemeKraj")).sendKeys("2812\t2020\t2150");// 28 Dec 2020, 21:50
        Thread.sleep(2000);
        driver.findElement(By.id("createButton")).click();
        Thread.sleep(3000);
        assertEquals("Festival: " + imeFestivala, driver.findElement(By.id("title")).getText());
        assertEquals("Voditelj: " + imeVoditelja, driver.findElement(By.id("leader")).getText());
    }

    @Test
    public void stvaranjePrijavniceZaDogadjajTest() throws InterruptedException {
        String imeFest = "fest1";

        Thread.sleep(2000);
        driver.findElement(By.id("korisnickoIme")).click();
        driver.findElement(By.id("korisnickoIme")).sendKeys("o");
        driver.findElement(By.id("password")).sendKeys("o");
        Thread.sleep(2000);
        driver.findElement(By.id("loginButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("festivals")).click();
        Thread.sleep(2000);
        driver.findElement(By.linkText(imeFest)).findElement(By.xpath("./../button[text()='Pogledaj']")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("appButton")).click();
        Thread.sleep(2000); // jer se alert ne pojavi odmah TODO
        String alertText = driver.switchTo().alert().getText();
        assertEquals("Uspješno stvorena prijava!", alertText);
        driver.switchTo().alert().accept();
        Thread.sleep(2000);
        driver.findElement(By.id("myProfile")).click();
        Thread.sleep(2000);
        assertDoesNotThrow(() -> driver.findElement(By.id("ul_prijavnice")).findElement(By.linkText(imeFest)));

        driver.findElement(By.linkText(imeFest)).findElement(By.xpath("./../button[text()='Ponisti']")).click(); // izbrisi napravljeno
    }

    @Test
    public void dodavanjeDjelatnostiTest() throws InterruptedException {
        String username = "kiki";
        String pass = "kiki";
        String djelatnost = "slasticarAlpha" + (int) (Math.random() * 1000);

        driver.findElement(By.id("korisnickoIme")).click();
        driver.findElement(By.id("korisnickoIme")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(pass);
        driver.findElement(By.id("loginButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("myProfile")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("createButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("ime")).click();
        driver.findElement(By.id("ime")).sendKeys(djelatnost);
        Thread.sleep(2000);
        driver.findElement(By.id("createButton")).click();
        Thread.sleep(2000);
        assertEquals("Djelatnost " + djelatnost + " stvorena i dodana.", driver.switchTo().alert().getText());
    }
}
