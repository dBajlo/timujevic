-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: data
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity` (
  `activity_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(255) NOT NULL,
  PRIMARY KEY (`activity_id`),
  UNIQUE KEY `UK_5r4i41tgx3i6b91p5wvsmfqvp` (`activity_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` VALUES (2,'Pjevac'),(1,'Ribar');
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) NOT NULL,
  `job_id` bigint(20) NOT NULL,
  `time` datetime(6) NOT NULL,
  `commenter_id` bigint(20) NOT NULL,
  `profile_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKm22n1cj5x3dj6josij9qyvt8m` (`commenter_id`),
  KEY `FK5og23425l8qhbophes4cjp66s` (`profile_id`),
  CONSTRAINT `FK5og23425l8qhbophes4cjp66s` FOREIGN KEY (`profile_id`) REFERENCES `registered_user` (`id`),
  CONSTRAINT `FKm22n1cj5x3dj6josij9qyvt8m` FOREIGN KEY (`commenter_id`) REFERENCES `registered_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,'Dobro odrađen posao!',1,'2020-01-16 13:06:46.309582',4,5);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_time` datetime(6) NOT NULL,
  `event_description` varchar(255) DEFAULT NULL,
  `event_name` varchar(255) NOT NULL,
  `festival_id` bigint(20) NOT NULL,
  `start_time` datetime(6) NOT NULL,
  `organizer_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrou6by4mvkypiblbh67bdc9nm` (`organizer_id`),
  CONSTRAINT `FKrou6by4mvkypiblbh67bdc9nm` FOREIGN KEY (`organizer_id`) REFERENCES `registered_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'2020-01-16 13:02:00.000000','nema opisa','Gradelada',1,'2020-01-16 13:01:00.000000',4),(2,'2020-01-19 11:30:00.000000','nema opisa','Grudanje',2,'2020-01-17 23:30:00.000000',4);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `festival`
--

DROP TABLE IF EXISTS `festival`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `festival` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `end_time` datetime(6) NOT NULL,
  `festival_name` varchar(255) NOT NULL,
  `logo` tinyblob,
  `start_time` datetime(6) NOT NULL,
  `leader_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgrd050pii23p0jd6selpiuj0b` (`leader_id`),
  CONSTRAINT `FKgrd050pii23p0jd6selpiuj0b` FOREIGN KEY (`leader_id`) REFERENCES `registered_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `festival`
--

LOCK TABLES `festival` WRITE;
/*!40000 ALTER TABLE `festival` DISABLE KEYS */;
INSERT INTO `festival` VALUES (1,'Morska tematika','2020-01-16 13:05:00.000000','Festival mora',NULL,'2020-01-16 13:00:00.000000',3),(2,'Zima','2020-01-20 12:45:00.000000','Zimski festival',NULL,'2020-01-16 23:00:00.000000',3),(3,'','2020-01-30 23:00:00.000000','Proljetni festival',NULL,'2020-01-28 23:00:00.000000',3);
/*!40000 ALTER TABLE `festival` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auction_end_time` datetime(6) DEFAULT NULL,
  `auction_open` bit(1) NOT NULL,
  `event_id` bigint(20) NOT NULL,
  `ordinal_number` int(11) NOT NULL,
  `win_app_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` VALUES (1,'2020-01-17 13:04:58.588769',_binary '',1,1,1),(2,'2020-01-17 13:12:55.435702',_binary '\0',2,1,2);
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_activity`
--

DROP TABLE IF EXISTS `job_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_id` bigint(20) NOT NULL,
  `job_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_activity`
--

LOCK TABLES `job_activity` WRITE;
/*!40000 ALTER TABLE `job_activity` DISABLE KEYS */;
INSERT INTO `job_activity` VALUES (1,1,1),(2,2,1),(3,1,2),(4,2,2);
/*!40000 ALTER TABLE `job_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_application`
--

DROP TABLE IF EXISTS `job_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_application` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) DEFAULT NULL,
  `confirmed` bit(1) NOT NULL,
  `job_duration` int(11) NOT NULL,
  `job_id` bigint(20) NOT NULL,
  `num_printed` int(11) NOT NULL,
  `num_workers` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `performer_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7y39djsrspkhb080nnl5fxmub` (`performer_id`),
  CONSTRAINT `FK7y39djsrspkhb080nnl5fxmub` FOREIGN KEY (`performer_id`) REFERENCES `registered_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_application`
--

LOCK TABLES `job_application` WRITE;
/*!40000 ALTER TABLE `job_application` DISABLE KEYS */;
INSERT INTO `job_application` VALUES (1,'Komentar.',_binary '',2,1,0,3,2,5),(2,'Komentar',_binary '',1,2,1,2,5,5);
/*!40000 ALTER TABLE `job_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization_application`
--

DROP TABLE IF EXISTS `organization_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organization_application` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `confirmed` bit(1) NOT NULL,
  `organizer_id` bigint(20) NOT NULL,
  `festival_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKslok7ry5rya8wv448fqv6txwu` (`festival_id`),
  CONSTRAINT `FKslok7ry5rya8wv448fqv6txwu` FOREIGN KEY (`festival_id`) REFERENCES `festival` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization_application`
--

LOCK TABLES `organization_application` WRITE;
/*!40000 ALTER TABLE `organization_application` DISABLE KEYS */;
INSERT INTO `organization_application` VALUES (1,_binary '',4,1),(2,_binary '',4,2),(3,_binary '\0',4,3);
/*!40000 ALTER TABLE `organization_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performer_activity`
--

DROP TABLE IF EXISTS `performer_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `performer_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_id` bigint(20) NOT NULL,
  `performer_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performer_activity`
--

LOCK TABLES `performer_activity` WRITE;
/*!40000 ALTER TABLE `performer_activity` DISABLE KEYS */;
INSERT INTO `performer_activity` VALUES (1,1,5),(2,2,5);
/*!40000 ALTER TABLE `performer_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registered_user`
--

DROP TABLE IF EXISTS `registered_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registered_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `profile_picture` tinyblob,
  `role` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_4qkjfwb01bh3q0r5x8uy5o67v` (`email`),
  UNIQUE KEY `UK_bpcdoul8n7slk4jjrymfe5oa4` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registered_user`
--

LOCK TABLES `registered_user` WRITE;
/*!40000 ALTER TABLE `registered_user` DISABLE KEYS */;
INSERT INTO `registered_user` VALUES (1,'admin@email.com','Dragan','Markić','$2a$10$rSES25.M6xB4iASmVD.v1ewelP5b.g03GroEX9loqxeHnnoivHiF2','0995555555',NULL,'admin','admin'),(3,'vspal@email.com','Vojko','Spaleta','$2a$10$UMzPE7kqry6nIRR5F9KgLOVgTa5b6HRe8Bg27eDy9.Cu9VRKcloUW','0982346543',NULL,'voditelj','Vojko'),(4,'oli@email.com','Oliver','Miletic','$2a$10$vvwBNxfEdyFLJjZ7dEgCgO5hRd3/wqsFHfBkPMhkXYsxISnpwUWzC','0956547890',NULL,'organizator','Oli'),(5,'pp@email.com','Pero','Peric','$2a$10$0FjJJmByV4ySNNUTlo01uu5uDVklH.1IR8aw1H8IUmk0jHlt4Zu4q','0984567985',NULL,'izvodjac','pero');
/*!40000 ALTER TABLE `registered_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-16 14:59:12
